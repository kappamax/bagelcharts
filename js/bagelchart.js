// JavaScript Document
(function($) {
	var defaultSettings = {
		'width' : 70,
		'size' : 100,
		'fgColor' : '#72debc',
		'bgColor' : '#ecedde',
		'fontFamily' : 'Verdana,arial,sans-serif',
		'fontColor' : '#7f7d7d',
		'fontWeight' : 'bold',
		'fontSize' : 12,
		'outerGradient' : true,
		'innerCircleGradientStartColor' : 'white',
		'innerCircleGradientEndColor' : 'white',
		'outerCircleGradientStartColor' : '#8ED6FF',
		'outerCircleGradientEndColor' : '#004CB3',
		'innerBorderWidth' : 0,
		'innerBorderColor' : '#888',
		'outerBorderWidth' : 0,
		'outerBorderColor' : '#888',
		'animationSpeed' : 50
	};

	var methods = {
		init : function(chartName, options) {
			var initcanvas = true;

			if (typeof (options) == "object") {
				this.settings = $.extend({}, defaultSettings, options);

				// autoscale width and fontSize
				if (options["size"] && !options["width"])
					this.settings["width"] = options["size"] / 4;
				this.settings["fontSize"] = this.settings["size"]
						- this.settings["width"] * 2 - this.settings["size"]
						/ 3;
			} else {
				if (typeof (this.settings) == "object")
					initcanvas = false;
				else
					this.settings = defaultSettings;
			}

			if (options["fgColor"]) {
				this.settings["outerGradient"] = false;
			}

			var size = this.settings.size
					+ (this.settings.outerBorderWidth * 2)
					+ (this.settings.innerBorderWidth * 2);
			this.settings["center"] = this.settings.size / 2;
			this.settings["radius"] = (this.settings.size - (this.settings.width * 2)) / 2;

			if (initcanvas) {
				$(this).css("position", "relative");
				$(this).css("width", size + "px");
				$(this).css("height", size + "px");
				$(this)
						.html(
								"<canvas id='"
										+ chartName
										+ "' width='"
										+ size
										+ "' height='"
										+ size
										+ "'></canvas>"
										+ "<div style='position:absolute;padding-top:0;z-index:10;text-align:center;width:"
										+ size + "px;font-family:"
										+ this.settings.fontFamily
										+ ";font-size:"
										+ this.settings.fontSize
										+ "px;font-weight:bold;'></div>");

				var canvas = $("canvas", this).get(0);

				// excanvas support
				if (typeof (G_vmlCanvasManager) != "undefined")
					G_vmlCanvasManager.initElement(canvas);

				var context = canvas.getContext('2d');

				methods.drawBackgroundCircle.call(context, this.settings);
			}
		},

		drawBackgroundCircle : function(settings, percent, current) {
			var showOuterBorder = (settings.outerBorderWidth > 0);
			var showInnerBorder = (settings.innerBorderWidth > 0);

			if (typeof percent == 'undefined')
				this.clearRect(0, 0, settings.size, settings.size);

			current = typeof current != 'undefined' ? current : 0;
			percent = typeof percent != 'undefined' ? percent : 100;

			var startAngle = degrees2Radians(percent2Degrees(current + 1));
			var endAngle = degrees2Radians(percent2Degrees(percent));
			var innerStartAngle = degrees2Radians(0);
			var innerEndAngle = degrees2Radians(percent2Degrees(100));

			// Draw outer circle/arc
			this.beginPath();
			this.fillStyle = settings.bgColor;
			this.arc(settings.center, settings.center, settings.center,
					startAngle, endAngle, false);
			if (showOuterBorder) {
				this.lineWidth = settings.outerBorderWidth;
				this.strokeStyle = settings.outerBorderColor;
				this.stroke();
			}
			this.fill();

			// Draw inner circle
			this.beginPath();
			var gradient = this.createLinearGradient(settings.center, settings.center - settings.radius,
					settings.center, settings.center
							+ settings.radius);
			gradient.addColorStop(0, settings.innerCircleGradientStartColor);
			gradient.addColorStop(0.65, settings.innerCircleGradientStartColor);
			gradient.addColorStop(1, settings.innerCircleGradientEndColor);
			this.fillStyle = gradient;

			this.arc(settings.center, settings.center, settings.radius
					- this.lineWidth + 1, innerStartAngle, innerEndAngle, true);
			if (showInnerBorder) {
				this.strokeStyle = settings.innerBorderColor;
				this.lineWidth = settings.innerBorderWidth;
				this.stroke();
			}
			this.fill();
		},

		drawForegroundCircle : function(settings, percent) {
			if (percent > 0) {
				var startAngle = degrees2Radians(0);
				var endAngle = degrees2Radians(percent2Degrees(percent));

				this.beginPath();
				if ( settings.outerGradient )
				{
					// TODO I'm not sure why the radial gradient fails in firefox - temporarily replaced with linear gradient
					/*var gradient = this.createRadialGradient(settings.center,
					settings.center, settings.width - 20, settings.center
							- settings.radius, settings.center
							+ settings.radius, (settings.width - 20) / 2);*/
					var gradient = this.createLinearGradient(settings.center + settings.radius, settings.center - settings.radius,
					settings.center - settings.radius, settings.center
							+ settings.radius);
			
					gradient
							.addColorStop(0, settings.outerCircleGradientStartColor);
					gradient
					.addColorStop(0.5, settings.outerCircleGradientStartColor);
					gradient.addColorStop(1, settings.outerCircleGradientEndColor);
					this.fillStyle = gradient;
				}
				else
				{
					this.fillStyle = settings.fgColor;
				}

				this.arc(settings.center, settings.center, settings.center,
						startAngle, endAngle, false);

				this.arc(settings.center, settings.center, settings.radius,
						endAngle, startAngle, true);
				this.fill();
			}
		},
	};

	$.fn.styleBagelChart = function(chartName, styles) {
		return this.each(function() {
			methods.init.call(this, chartName, styles);
		});
	};

	$.fn.createBagelChart = function(method, previousPercent) {
		return this.each(function() {
			if (method == "draw") {
				var percentage = $(this).attr("data-percent");
				var canvas = $(this).children("canvas").get(0);
				var chartSettings = this.settings;

				if (canvas.getContext) {
					var context = canvas.getContext('2d');
					var animationID = null;
					var current = previousPercent;

					function decrement() {
						current = normalizePercentage(current - 1);
						methods.drawBackgroundCircle.call(context,
								chartSettings, percentage, current);
						methods.drawForegroundCircle.apply(context, [
								chartSettings, current ]);

						drawNumber();

						if (percentage >= current) {
							clearInterval(animationID);
						}
					}

					function increment() {
						current = normalizePercentage(current + 1);

						methods.drawBackgroundCircle.call(context,
								chartSettings);
						methods.drawForegroundCircle.apply(context, [
								chartSettings, current ]);

						drawNumber();

						if (current >= percentage)
							clearInterval(animationID);
					}

					function drawNumber() {
						context.fillStyle = chartSettings.fontColor;
						context.textAlign = 'center';
						context.font = chartSettings.fontWeight + " "
								+ chartSettings.fontSize + "px "
								+ chartSettings.fontFamily;
						// context.shadow = "0px 1px 1px #777";
						var centerPoint = chartSettings.size / 2;

						context.fillText(current, centerPoint, centerPoint
								+ chartSettings.fontSize / 2.6);
					}

					if (percentage > previousPercent)
						animationID = setInterval(increment,
								chartSettings.animationSpeed);
					else if (previousPercent > percentage)
						animationID = setInterval(decrement,
								chartSettings.animationSpeed);
				}
			}
		});
	};
})(jQuery);

function degrees2Radians(degrees) {
	return (Math.PI * (degrees - 90)) / 180;
}

function percent2Degrees(percent) {
	return percent / 100 * 360;
}

function normalizePercentage(percent) {
	if (percent <= 0)
		return 0;
	else if (percent >= 100)
		return 100;
	return percent;
}