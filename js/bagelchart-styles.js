var styles = {
	'Blue' : {
		'size' : 182,
		'bgColor' : '#e9e9e9',
		'innerCircleGradientEndColor' : '#d9d8d8',
		'outerCircleGradientStartColor' : '#5195c7',
		'outerCircleGradientEndColor' : '#32bfc6',
		'width' : 36,
		'innerBorderColor' : '#c0c0c0',
		'innerBorderWidth' : 2,
		'fontColor': '#686868',
		'animationSpeed' : 50
	},
	'Red' : {
		'size' : 200,
		'fgColor' : '#3c3679',
		'bgColor' : '#9490ba',
		'fontFamily' : 'serif',
		'innerCircleGradientEndColor' : '#bdb0c5',
		'outerBorderColor' : '#380b54',
		'innerBorderColor' : 'gray',
		'innerBorderWidth' : 2
	}
};